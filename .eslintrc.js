module.exports = {
	'env': {
		'browser': true,
		'es6': true
	},
	'extends': 'airbnb',
	"parser": "babel-eslint",
	'parserOptions': {
		'ecmaFeatures': {
			'experimentalObjectRestSpread': true,
			'jsx': true
		},
		'sourceType': 'module'
	},
	'plugins': [
		'react',
		"import"
	],
	'rules': {
		'indent': ['error', 'tab'],
		'linebreak-style': ['error', 'windows'],
		'react/jsx-filename-extension': [1, { 'extensions': ['.js', '.jsx'] }],
		'no-tabs': 0,
		'no-console': 0,
		'arrow-parens': 0,
		'react/jsx-indent': 0,
		'react/jsx-indent-props': 0,
		'max-len': 0,
		'comma-dangle': 0,
		'prefer-template': 0,
		'react/jsx-closing-bracket-location': 0,
		'no-param-reassign': 0,
		'no-mixed-operators': 0,
		'react/jsx-no-bind': 0,
		'react/forbid-prop-types': 0,
		'function-paren-newline': 0,
		'react/require-default-props': 0,
		'object-curly-newline': 0,
		'no-trailing-spaces': 0
	}
};
