import PropTypes from 'prop-types';

const PropType = {};

Object.assign(PropType, {
	/** pokémon's stat */
	Stat: PropTypes.shape({
		/** stat id */
		id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
		name: PropTypes.string,
		value: PropTypes.number
	}),

	Type: PropTypes.shape({
		/** type id */
		id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
		name: PropTypes.string
	}),

	/** pokémon's ability */
	Ability: PropTypes.shape({
		/** ability id */
		id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
		name: PropTypes.string,
		hidden: PropTypes.bool
	}),

	MyPokemonStore: PropTypes.shape({
		/** pokémon ids marked as my */
		myPokemonIds: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.number), PropTypes.object])
	})
});
Object.assign(PropType, {
	Move: PropTypes.shape({
		/** move id */
		id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
		/** move name */
		name: PropTypes.string,
		types: PropTypes.arrayOf(PropType.Type),
		pp: PropTypes.number,
		accuracy: PropTypes.number,
		power: PropTypes.number,
		effectChance: PropTypes.number,
		damageClass: PropTypes.string,
		description: PropTypes.string,
		shortEffect: PropTypes.string,
		effect: PropTypes.string
	})
});
Object.assign(PropType, {
	Pokemon: PropTypes.shape({
		/** pokémon's id */
		id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
		name: PropTypes.string,
		types: PropTypes.oneOfType([PropTypes.arrayOf(PropType.Type), PropTypes.object]),
		moves: PropTypes.arrayOf(PropType.Move),
		abilities: PropTypes.arrayOf(PropType.Ability),
		stats: PropTypes.arrayOf(PropType.Stat),
		maxStat: PropTypes.number,
		species: PropTypes.string,
		description: PropTypes.string,
		height: PropTypes.number,
		weight: PropTypes.number
	}),

	MovedexStore: PropTypes.shape({
		type: PropTypes.string,
		search: PropTypes.string,
		currentPage: PropTypes.number,
		perPage: PropTypes.number,
		selectedMove: PropType.Move,
		/** array of moves ( PropTypes.arrayOf(Utils.PropType.Move) ), object because of MobXObservableObject */
		moves: PropTypes.object,
		getMoveDetails: PropTypes.func
	})
});
Object.assign(PropType, {
	PokedexStore: PropTypes.shape({
		type: PropTypes.string,
		search: PropTypes.string,
		currentPage: PropTypes.number,
		perPage: PropTypes.number,
		pokemon: PropTypes.oneOfType([PropTypes.arrayOf(PropType.Pokemon), PropTypes.object]),
		selectedPokemon: PropType.Pokemon,
		totalNofTypes: PropTypes.number,
		NofTypes: PropTypes.number,
		myPokemon: PropTypes.bool
	})
});

export { PropType };

export const getIdFromUrl = url => url.replace(/https:\/\/pokeapi.co\/api\/v2\/[a-z-]+\//i, '').replace('/', '');

export const colors = {
	normal: '#BFB97F',
	fighting: '#D32F2E',
	flying: '#9E87E1',
	poison: '#AA47BC',
	ground: '#DFB352',
	rock: '#BDA537',
	bug: '#98B92E',
	ghost: '#7556A4',
	steel: '#B4B4CC',
	fire: '#E86513',
	water: '#2196F3',
	grass: '#4CB050',
	electric: '#FECD07',
	psychic: '#EC407A',
	ice: '#80DEEA',
	dragon: '#673BB7',
	dark: '#5D4038',
	fairy: '#F483BB'
};

export const getColorFromType = type => colors[type.toLowerCase()] || '#CBD4DD';

export const getColorFromDamageClass = damageClass => {
	const dcolors = {
		status: '#808080',
		physical: '#B30000',
		special: '#3366CC'
	};

	return dcolors[damageClass.toLowerCase()] || '#CBD4DD';
};

export const darkenHexColor = color => {
	const letters = '0123456789ABCDEF';
	return color
		.toUpperCase()
		.split('')
		.map(v => {
			const i = letters.indexOf(v);
			if (i <= 0) return v;
			if (i === 1) return letters[0];
			return letters[i - 2];
		})
		.join('');
};

export const lightenHexColor = color => {
	const letters = '0123456789ABCDEF';
	return color
		.toUpperCase()
		.split('')
		.map(v => {
			const i = letters.indexOf(v);
			if (i < 0 || i >= 15) return v;
			if (i === 14) return letters[15];
			return letters[i + 2];
		})
		.join('');
};

const def = 'def';
export default def;
