import React from 'react';
import { observer } from 'mobx-react';

import Navbar from './components/Navbar';
import Pokedex from './pages/Pokedex';
import Movedex from './pages/Movedex';

import PokedexStore from './stores/PokedexStore';
import MovedexStore from './stores/MovedexStore';
import MyPokemonStore from './stores/MyPokemonStore';
import PageStore from './stores/PageStore';

@observer
export default class App extends React.Component {
	myPokemonStore = new MyPokemonStore();
	pokedexStore = new PokedexStore(this.myPokemonStore);
	movedexStore = new MovedexStore();
	pageStore = new PageStore(this.pokedexStore);
	render() {
		return (
			<div>
				{this.pageStore.page === 'Movedex' && <Navbar pageStore={this.pageStore} title="Movedex" />}
				{this.pageStore.page === 'Pokedex' && <Navbar pageStore={this.pageStore} title="Pokédex" />}
				{this.pageStore.page === 'MyPokemon' && <Navbar pageStore={this.pageStore} title="My Pokémon" />}
				<br />
				{this.pageStore.page !== 'Movedex' && <Pokedex store={this.pokedexStore} myPokemonStore={this.myPokemonStore} />}
				{this.pageStore.page === 'Movedex' && <Movedex store={this.movedexStore} />}
			</div>
		);
	}
}
