import React from 'react';
import { observer } from 'mobx-react';

import { Pokedex as Pokeapi } from '../pokeapi-js-wrapper/index';

import Pagination from '../components/Pagination';
import TypeSelectDropdown from '../components/TypeSelectDropdown';
import PokemonSummaryCard from '../components/pokedex/PokemonSummaryCard';
import PokemonCardModal from '../components/pokedex/PokemonCardModal';
import MovedexStore from '../stores/MovedexStore';
import * as Utils from '../utils/Utils';

/** ### Pokédex
 * Container for pokemon cards.
 */
@observer
export default class Pokedex extends React.Component {
	static propTypes = {
		myPokemonStore: Utils.PropType.MyPokemonStore,
		store: Utils.PropType.PokedexStore
	};
	async componentDidMount() {
		const { store } = this.props;
		await store.getPokemon(this.P);
		this.alertMessage = 'No pokemon found.';
		this.forceUpdate();
	}
	alertMessage = <div className="loader" />;
	movedexStore = null;
	P = new Pokeapi({
		protocol: 'https',
		versionPath: '/api/v2/',
		cache: true,
		timeout: 20 * 1000
	});
	searchChange(e) {
		const { store } = this.props;
		store.search = e.target.value;
		store.currentPage = 1;
	}
	clearSearch(searchInput) {
		const { store } = this.props;
		searchInput.value = '';
		store.search = '';
		store.currentPage = 1;
	}
	async openPokemon(id, ref) {
		const { store } = this.props;
		await store.selectPokemon(this.P, id, ref);
		this.movedexStore = new MovedexStore(store.selectedPokemon.moves);
		await this.movedexStore.getMoves(this.P);
		this.forceUpdate();
	}
	closePokemon() {
		this.props.store.selectedPokemon = null;
	}
	render() {
		const { myPokemonStore, store } = this.props;
		const searchInput = React.createRef();

		const pokemonCards = store.paginated.map(pokemon => {
			if (!pokemon.types || pokemon.types.length === 0) return null;
			return (
				<PokemonSummaryCard
					className="col-lg-6 col-md-12 fadein"
					key={pokemon.id}
					myPokemonStore={myPokemonStore}
					{...pokemon}
					onClick={() => this.openPokemon.bind(this)(pokemon.id, this)}
				/>
			);
		});

		return (
			<div>
				<PokemonCardModal movedexStore={this.movedexStore} myPokemonStore={myPokemonStore} {...store.selectedPokemon} onClose={this.closePokemon.bind(this)} />

				<div className="container-fluid">
					{/* <div style={{ marginBottom: '5px' }}>
						<button
							className={'btn btn' + (!store.myPokemon ? '-outline' : '') + '-warning'}
							style={{ color: '#000000' }}
							onClick={() => {
								store.myPokemon = !store.myPokemon;
								store.currentPage = 1;
							}}>
							My pokemon {store.myPokemon ? '✓' : ''}
						</button>
						</div> */}
					<div className="input-group mb-3">
						<div className="input-group-prepend">
							<TypeSelectDropdown store={store} />
						</div>
						<input ref={searchInput} className="form-control" placeholder="# or name" onChange={this.searchChange.bind(this)} />
						<div className="input-group-append">
							<button className="btn btn-light border" onClick={() => this.clearSearch.bind(this)(searchInput.current)}>
								x
							</button>
						</div>
					</div>
				</div>

				<div className="container-fluid">
					{(this.alertMessage[0] !== 'N' && (
						<div className="text-center">
							<div className="progress bg-dark" style={{ height: '30px' }}>
								<div
									className="progress-bar progress-bar-striped progress-bar-animated position-relative bg-primary"
									role="progressbar"
									style={{ width: (store.NofTypes / store.totalNofTypes * 100).toString() + '%' }}
								/>
								<small className="justify-content-center d-flex position-absolute w-100 flex-row align-items-center text-light" style={{ fontSize: '20px' }}>
									<b>Loading</b>
								</small>
							</div>
							<br />
						</div>
					)) ||
						(pokemonCards.length === 0 && <div className="alert alert-info">{this.alertMessage}</div>)}
				</div>
				<div className="container-fluid">
					{pokemonCards.length > 0 && <Pagination store={store} />}
					<div className="row">{pokemonCards}</div>
					<br />
					{pokemonCards.length > 5 && <Pagination store={store} />}
					<br />
				</div>
			</div>
		);
	}
}
