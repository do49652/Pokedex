import React from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';

import { Pokedex as Pokeapi } from '../pokeapi-js-wrapper/index';

import Pagination from '../components/Pagination';
import TypeSelectDropdown from '../components/TypeSelectDropdown';
import MoveSummaryCard from '../components/movedex/MoveSummaryCard';
import * as Utils from '../utils/Utils';

/** ### Movedex
 * Container for move cards.
 */
@observer
export default class Movedex extends React.Component {
	static propTypes = {
		className: PropTypes.string,
		store: Utils.PropType.MovedexStore
	};
	componentDidMount() {
		const { store } = this.props;
		if (store) store.getMoves(this.P);
	}
	P = new Pokeapi({
		protocol: 'https',
		versionPath: '/api/v2/',
		cache: true,
		timeout: 20 * 1000
	});
	searchChange(e) {
		const { store } = this.props;
		store.search = e.target.value;
		store.currentPage = 1;
	}
	clearSearch(searchInput) {
		const { store } = this.props;
		searchInput.value = '';
		store.search = '';
		store.currentPage = 1;
	}
	render() {
		const { className, store } = this.props;
		if (!store) return null;
		const searchInput = React.createRef();
		const movesCards = store.paginated.map(move => {
			if (move.types.length === 0) return null;
			return <MoveSummaryCard className={className || 'col-lg-6 col-md-12 fadein'} key={move.id} id={move.id} store={store} P={this.P} />;
		});

		return (
			<div>
				<div className="container-fluid">
					<div className="input-group mb-3">
						<div className="input-group-prepend">
							<TypeSelectDropdown store={store} />
						</div>
						<input ref={searchInput} className="form-control" placeholder="# or name" onChange={this.searchChange.bind(this)} />
						<div className="input-group-append">
							<button className="btn btn-light border" onClick={() => this.clearSearch.bind(this)(searchInput.current)}>
								x
							</button>
						</div>
					</div>
				</div>

				<div className="container-fluid">
					{movesCards.length === 0 && <div className="alert alert-info">No moves found.</div>}
					{movesCards.length > 0 && <Pagination store={store} />}
					<div className="row">{movesCards}</div>
					{movesCards.length > 5 && <Pagination store={store} />}
					<br />
				</div>
			</div>
		);
	}
}
