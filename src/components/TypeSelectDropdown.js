import React from 'react';
import PropTypes from 'prop-types';

import * as Utils from '../utils/Utils';

/** ### TypeSelect
 * Dropdown for type filtering.
 */
export default class TypeSelectDropdown extends React.Component {
	static propTypes = {
		store: PropTypes.oneOfType([Utils.PropType.MovedexStore, Utils.PropType.PokedexStore])
	};
	changeType(type) {
		const { store } = this.props;
		store.type = type;
		store.currentPage = 1;
	}
	render() {
		const { store } = this.props;
		const changeType = this.changeType.bind(this);

		let type = store.type || 'All types';
		type = type[0].toUpperCase() + type.substring(1);

		return (
			<div>
				<button
					className="btn border border-dark dropdown-toggle"
					type="button"
					data-toggle="dropdown"
					style={{
						backgroundColor: Utils.getColorFromType(type)
					}}>
					{type}
				</button>
				<div className="dropdown-menu scroll-50vh" style={{ backgroundColor: '#1e5799' }}>
					<div className="container-fluid">
						<button className="btn btn-dark w-100" onClick={() => changeType(null)}>
							All types
						</button>
						<div role="separator" className="dropdown-divider" />

						{Object.keys(Utils.colors)
							.map(c => c[0].toUpperCase() + c.substring(1))
							.map(t => (
								<div key={'typecolor_' + t}>
									<button
										className="btn w-100 hoverDarken"
										style={{ backgroundColor: Utils.darkenHexColor(Utils.getColorFromType(t.toLowerCase())), marginBottom: '5px', color: '#FFFFFF' }}
										onClick={() => changeType(t.toLowerCase())}>
										<b>{t}</b>
									</button>
									<br />
								</div>
							))}
					</div>
				</div>
			</div>
		);
	}
}
