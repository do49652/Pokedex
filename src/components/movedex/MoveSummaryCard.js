import React from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';

import TableCoollapse from '../TableCoollapse';
import * as Utils from '../../utils/Utils';

/** ### Move card
 * Shows *Move ID* and *Name*, background based on *Type*.  
 * Content shown with **TableCoollapse**, contains *Power*, *Accuracy*, *PP*, *Description* and *Effect*.
 */
@observer
export default class MoveSummaryCard extends React.Component {
	static propTypes = {
		/** move id */
		id: PropTypes.string,
		className: PropTypes.string,
		/** pokeapi */
		P: PropTypes.object,
		store: Utils.PropType.MovedexStore
	};
	async fetchMove(id) {
		await this.props.store.getMoveDetails(this.props.P, id);
		this.forceUpdate();
	}
	render() {
		const { id, store } = this.props;
		const move = store.moves.find(m => m.id === id);

		return (
			<div className={this.props.className}>
				<div
					className="card flex-row flex-wrap"
					style={{
						margin: '5px',
						backgroundColor: Utils.getColorFromType(move.types[0].name),
						backgroundImage:
							'-webkit-linear-gradient(-40deg, ' +
							Utils.getColorFromType(move.types[0].name) +
							' 50%, ' +
							(move.types.length === 1 ? Utils.lightenHexColor(Utils.getColorFromType(move.types[0].name)) : Utils.getColorFromType(move.types[1].name)) +
							' 50%)',
						borderColor: '#000000'
					}}>
					<div className="card-block w-100" style={{ margin: '10px', marginBottom: 0 }}>
						<div className="row w-100 card-title">
							<div className="col">
								<h4>
									<span className="text-light">#{move.id}</span> {move.name[0].toUpperCase() + move.name.substring(1)}
								</h4>
							</div>

							<div className="col-4">
								<div className="w-100" style={{ marginBottom: '12px', marginTop: '5px' }}>
									<div className="row">
										{move.types.map(t => (
											<div key={'type_' + t.id + '_for_' + move.id}>
												<span
													className="btn-sm border border-dark w-100"
													style={{
														backgroundColor: Utils.darkenHexColor(Utils.getColorFromType(t.name)),
														color: '#FFFFFF',
														marginLeft: '30px'
													}}>
													<b>{t.name.toUpperCase()}</b>
												</span>
											</div>
										))}
									</div>
								</div>
							</div>
						</div>
					</div>

					<div className="card-text container-fluid" style={{ marginBottom: '10px' }}>
						<TableCoollapse
							id={'move_' + move.id}
							title="Info"
							onClick={() => this.fetchMove.bind(this)(move.id)}
							style={{ backgroundColor: Utils.darkenHexColor(Utils.getColorFromType(move.types[0].name)) }}>
							<div className="container-fluid">
								{move.damageClass && (
									<div className="container">
										<div className="text-center" style={{ margin: '10px' }}>
											<span
												className="btn-sm border border-dark w-100"
												style={{
													backgroundColor: Utils.darkenHexColor(Utils.getColorFromDamageClass(move.damageClass)),
													color: '#FFFFFF'
												}}>
												<b>{move.damageClass.toUpperCase()}</b>
											</span>
										</div>
										<p className="text-center text-light" style={{ fontSize: '70%', marginBottom: 0 }}>
											Damage
										</p>
										<hr style={{ marginTop: 0 }} />
									</div>
								)}

								<table className="table" style={{ backgroundColor: 'transparent', marginBottom: 0 }}>
									<thead>
										<tr>
											<th>Power</th>
											<th>Accuracy</th>
											<th>PP</th>
										</tr>
									</thead>
									{(move.loaded && (
										<tbody>
											<tr>
												<td>{move.power || '-'}</td>
												<td>{move.accuracy || '-'}</td>
												<td>{move.pp || '-'}</td>
											</tr>
											<tr>
												<td colSpan="3">
													<hr style={{ marginTop: 0 }} />
													<p className="text-center" style={{ margin: 0 }}>
														{move.description}
													</p>
													<p className="text-center text-light" style={{ fontSize: '70%', marginBottom: 0 }}>
														Description
													</p>
													<hr style={{ marginTop: 0 }} />
												</td>
											</tr>
											<tr>
												<td colSpan="3">
													<div className="card-text container-fluid" style={{ marginBottom: '10px', padding: 0 }}>
														<TableCoollapse
															id={'move_effect_' + move.id}
															title={move.shortEffect.length < move.effect.length ? move.shortEffect : move.effect}
															style={{ backgroundColor: Utils.darkenHexColor(Utils.getColorFromType(move.types[0].name)) }}>
															<div
																className="container-fluid"
																style={{ padding: '5px', backgroundColor: Utils.darkenHexColor(Utils.getColorFromType(move.types[0].name)) }}>
																{move.shortEffect.length < move.effect.length ? move.effect : move.shortEffect}
															</div>
														</TableCoollapse>
													</div>
													<p className="text-center text-light" style={{ fontSize: '70%', marginBottom: 0 }}>
														Effect
													</p>
													<hr style={{ marginTop: 0 }} />
												</td>
											</tr>
										</tbody>
									)) || (
										<tbody>
											<tr>
												<td colSpan="3">
													<div className="loader" />
												</td>
											</tr>
										</tbody>
									)}
								</table>
							</div>
						</TableCoollapse>
					</div>
				</div>
			</div>
		);
	}
}
