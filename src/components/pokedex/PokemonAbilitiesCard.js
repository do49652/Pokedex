import React from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';

import * as Utils from '../../utils/Utils';

/** ### Pokémon Abilities card
 * Shows pokémon's *Abilities*
 */
@observer
export default class PokemonAbilitiesCard extends React.Component {
	static propTypes = {
		className: PropTypes.string,
		/** array of pokémon's abilities */
		abilities: PropTypes.arrayOf(Utils.PropType.Ability),
		/** array of pokémon's types ( PropTypes.arrayOf(Utils.PropType.Type) ), object because of MobXObservableObject */
		types: PropTypes.object
	};
	render() {
		const { className, abilities, types } = this.props;
		return (
			<div className={className}>
				<div className="card flex-row flex-wrap zoom" style={{ margin: '5px' }}>
					<div className="card-block px-2 w-100" style={{ margin: '10px' }}>
						<h4 className="card-title">
							<span>Abilities</span>
						</h4>

						<div className="container-fluid w-100">
							{(abilities &&
								abilities.map(ability => (
									<div key={'ability_' + ability.id} className="progress position-relative w-100" style={{ height: '30px', marginBottom: '5px' }}>
										<div
											className="progress-bar text-light"
											role="progressbar"
											style={{ width: ability.hidden ? '20%' : '100%', height: '30px', backgroundColor: Utils.getColorFromType(types[0].name) }}>
											{ability.hidden && 'Hidden'}
										</div>
										<small
											className={
												'justify-content-center d-flex position-absolute w-100 flex-row align-items-center ' + (ability.hidden ? 'text-dark' : 'text-light')
											}
											style={{ fontSize: '20px' }}>
											{ability.name[0].toUpperCase() + ability.name.substring(1)}
										</small>
									</div>
								))) || <div className="loader" />}
						</div>
					</div>
				</div>
			</div>
		);
	}
}
