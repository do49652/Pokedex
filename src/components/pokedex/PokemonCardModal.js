import React from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';

import Movedex from '../../pages/Movedex';
import PokemonSummaryCard from './PokemonSummaryCard';
import PokemonInfoCard from './PokemonInfoCard';
import PokemonAbilitiesCard from './PokemonAbilitiesCard';
import PokemonStatsCard from './PokemonStatsCard';
import * as Utils from '../../utils/Utils';

/** ### Pokémon card modal
 * Modal with all pokemon's info.
 */
@observer
export default class PokemonCardModal extends React.Component {
	static propTypes = {
		/** pokémon's id */
		id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
		/** pokémon's name */
		name: PropTypes.string,
		/** array of pokémon's types ( PropTypes.arrayOf(Utils.PropType.Type) ), object because of MobXObservableObject */
		types: PropTypes.object,
		/** array of pokémon's moves names */
		moves: PropTypes.arrayOf(PropTypes.string),
		/** array of pokémon's abilities */
		abilities: PropTypes.arrayOf(Utils.PropType.Ability),
		/** array of pokémon's stats */
		stats: PropTypes.arrayOf(Utils.PropType.Stat),
		/** max value of pokémon's stats */
		maxStat: PropTypes.number,
		species: PropTypes.string,
		description: PropTypes.string,
		height: PropTypes.number,
		weight: PropTypes.number,
		className: PropTypes.string,
		myPokemonStore: Utils.PropType.MyPokemonStore,
		movedexStore: Utils.PropType.MovedexStore,
		/** run when modal closes */
		onClose: PropTypes.func
	};
	render() {
		const { props } = this;
		return (
			<div className="modal fade" id="pokemonCard" data-backdrop="static" data-keyboard="false" tabIndex="-1" >
				<div className="modal-dialog modal-lg modal-dialog-centered">
					<div className="modal-content">
						<div className="modal-header" style={{ margin: 0, padding: 0 }}>
							{props.id && <PokemonSummaryCard className="w-100" {...props} modal="true" myPokemonStore={props.myPokemonStore} />}
						</div>
						<div className="modal-body" style={{ margin: 0, padding: 0 }}>
							{!props.id && <div className="loader" />}
							{props.id && (
								<div>
									<div className="row">
										<div className="col" style={{ paddingRight: 0 }}>
											<ul className="nav nav-tabs">
												<li className="nav-item">
													<a className="nav-link active" data-toggle="tab" href="#info">
														<span>Info</span>
													</a>
												</li>
												<li className="nav-item">
													<a className="nav-link" data-toggle="tab" href="#moves">
														<span>Moves</span>
													</a>
												</li>
											</ul>
										</div>
										<div
											className="col border-bottom"
											style={{
												paddingLeft: 0,
												paddingRight: 0,
												marginRight: '30px',
												marginTop: '5px'
											}}>
											<div className="text-right">
												<button className="btn btn-sm btn-danger" data-dismiss="modal" onClick={props.onClose}>
													Close
												</button>
											</div>
										</div>
									</div>
									<div className="tab-content">
										<div className="tab-pane fade show active" id="info">
											<PokemonInfoCard className="w-100" {...props} />
											<PokemonAbilitiesCard className="w-100" {...props} />
											<PokemonStatsCard className="w-100" {...props} />
										</div>
										<div className="tab-pane fade" id="moves">
											<div className="scroll-70vh" style={{ marginTop: '5px' }}>
												{props.moves && <Movedex className="col-md-12 fadein" store={props.movedexStore} />}
											</div>
										</div>
									</div>
								</div>
							)}
						</div>
					</div>
				</div>
			</div>
		);
	}
}
