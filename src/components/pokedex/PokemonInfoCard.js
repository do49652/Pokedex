import React from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';

import * as Utils from '../../utils/Utils';

/** ### Pokémon info card
 * Shows pokémon's *species*, *type*, *description*, *height*, *weight*.
 */
@observer
export default class PokemonInfoCard extends React.Component {
	static propTypes = {
		className: PropTypes.string,
		species: PropTypes.string,
		/** array of pokémon's types ( PropTypes.arrayOf(Utils.PropType.Type) ), object because of MobXObservableObject */
		types: PropTypes.object,
		description: PropTypes.string,
		height: PropTypes.number,
		weight: PropTypes.number
	};
	render() {
		const { className, species, types, description, height, weight } = this.props;
		return (
			<div className={className}>
				<div className="card flex-row flex-wrap zoom" style={{ margin: '5px' }}>
					<div className="card-block px-2" style={{ margin: '10px' }}>
						<div className="container-fluid" style={{ padding: 0 }}>
							<div className="row">
								<div className="col">
									{(species && <span className="spn border w-100">{species}</span>) || <div className="loader" />}
									<p className="text-center text-muted" style={{ fontSize: '70%' }}>
										Species
									</p>
								</div>
								<div className="col">
									<div className="row">
										<div className="col">
											<span
												className="spn border w-100"
												style={{
													backgroundColor: Utils.darkenHexColor(Utils.getColorFromType(types[0].name)),
													color: '#FFFFFF'
												}}>
												{types[0].name[0].toUpperCase() + types[0].name.substring(1)}
											</span>
											<p className="text-center text-muted" style={{ fontSize: '70%' }}>
												Primary type
											</p>
										</div>
										{types.length > 1 && (
											<div className="col">
												<span
													className="spn border w-100"
													style={{
														backgroundColor: Utils.darkenHexColor(Utils.getColorFromType(types[1].name)),
														color: '#FFFFFF'
													}}>
													{types[1].name[0].toUpperCase() + types[1].name.substring(1)}
												</span>
												<p className="text-center text-muted" style={{ fontSize: '70%' }}>
													Secondary type
												</p>
											</div>
										)}
									</div>
								</div>
							</div>
							<hr style={{ marginTop: 0 }} />
						</div>

						{(description && (
							<p className="card-text" style={{ marginBottom: '5px' }}>
								{description}
							</p>
						)) || <div className="loader" />}
						<p className="text-center text-muted" style={{ fontSize: '70%' }}>
							Pokédex entry
						</p>
						<hr style={{ marginTop: 0 }} />

						<div className="container-fluid">
							<div className="row">
								<div className="col">
									{(height && (
										<p className="text-center" style={{ marginBottom: '5px' }}>
											{height} m
										</p>
									)) || <div className="loader" />}
									<p className="text-center text-muted" style={{ fontSize: '70%' }}>
										Height
									</p>
								</div>
								<div className="col">
									{(weight && (
										<p className="text-center" style={{ marginBottom: '5px' }}>
											{weight} m
										</p>
									)) || <div className="loader" />}
									<p className="text-center text-muted" style={{ fontSize: '70%' }}>
										Weight
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
