import React from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';

import * as Utils from '../../utils/Utils';

/** ### Pokémon stats card
 * Shows *speed*, *defence*, *attack*, *hp*, *special attack*, *special defense*.
 */
@observer
export default class PokemonStatsCard extends React.Component {
	static propTypes = {
		className: PropTypes.string,
		/** array of pokémon's stats */
		stats: PropTypes.arrayOf(Utils.PropType.Stat),
		/** max value of pokémon's stats */
		maxStat: PropTypes.number,
		/** array of pokémon's types ( PropTypes.arrayOf(Utils.PropType.Type) ), object because of MobXObservableObject */
		types: PropTypes.object
	};
	render() {
		const { className, stats, maxStat, types } = this.props;
		return (
			<div className={className}>
				<div className="card flex-row flex-wrap zoom" style={{ margin: '5px' }}>
					<div className="card-block px-2 w-100" style={{ margin: '10px' }}>
						<h4 className="card-title">
							<span>Stats</span>
						</h4>

						<div className="container-fluid w-100">
							{(stats &&
								stats.map(stat => (
									<div className="row" key={'stat_' + stat.id}>
										<div className="col-4 text-right" style={{ padding: 0 }}>
											{stat.name[0].toUpperCase() + stat.name.substring(1).replace('-', ' ')}
										</div>
										<div className="col">
											<div className="progress position-relative w-100.1" style={{ height: '30px', marginBottom: '5px' }}>
												<div
													className="progress-bar"
													role="progressbar"
													style={{
														width: (stat.value / maxStat * 100).toString() + '%',
														height: '30px',
														backgroundColor: Utils.getColorFromType(types[0].name)
													}}>
													<div className="w-100 text-right" style={{ fontSize: '20px', paddingRight: '10px' }}>
														{stat.value}
													</div>
												</div>
											</div>
										</div>
									</div>
								))) || <div className="loader" />}
							<div className="row">
								{(stats && <div className="col text-center">Total: {stats.map(s => s.value).reduce((p, c) => p + c)}</div>) || <div className="loader" />}
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
