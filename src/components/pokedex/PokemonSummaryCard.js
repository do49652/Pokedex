import React from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';

import * as Utils from '../../utils/Utils';

/** ### Pokémon card
 * Shows *Pokémon ID* and *Name*, background based on *Types*.
 * Button to add the pokémon to My Pokémon list.
 */
@observer
export default class PokemonSummaryCard extends React.Component {
	static propTypes = {
		/** pokémon's id */
		id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
		/** pokémon's name */
		name: PropTypes.string,
		className: PropTypes.string,
		/** is card viewed in modal */
		modal: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
		myPokemonStore: Utils.PropType.MyPokemonStore,
		/** array of pokémon's types ( PropTypes.arrayOf(Utils.PropType.Type) ), object because of MobXObservableObject */
		types: PropTypes.object,
		/** run when user clicks on the button */
		onClick: PropTypes.func
	};
	render() {
		const { className, types, modal, myPokemonStore, id, name, onClick } = this.props;
		return (
			<div className={className}>
				<div
					className="card flex-row flex-wrap"
					style={{
						margin: '5px',
						backgroundColor: Utils.getColorFromType(types[0].name),
						backgroundImage:
							'-webkit-linear-gradient(-40deg, ' +
							Utils.getColorFromType(types[0].name) +
							' 50%, ' +
							(types.length === 1 ? Utils.lightenHexColor(Utils.getColorFromType(types[0].name)) : Utils.getColorFromType(types[1].name)) +
							' 50%)',
						borderColor: '#000000',
						borderRadius: '10px'
					}}>
					{!modal && (
						<div className="pull-right text-right w-100" style={{ position: 'absolute', marginTop: '5px', paddingRight: '5px' }}>
							<button
								className={'btn btn-sm btn-' + (myPokemonStore.isMy(id) ? 'danger' : 'warning') + ' border border-dark'}
								onClick={() => {
									if (!myPokemonStore.isMy(id)) myPokemonStore.addPokemon(id);
									else myPokemonStore.removePokemon(id);
									this.forceUpdate();
								}}>
								{myPokemonStore.isMy(id) ? '-' : '+'}
							</button>
						</div>
					)}

					<div
						className="card-header"
						style={myPokemonStore.isMy(id) ? { border: 0, borderLeftStyle: 'solid', borderLeftWidth: '15px', borderLeftColor: '#FFC107', borderRadius: '10px' } : {}}>
						<img src={'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/' + id + '.png'} alt={name} />
					</div>

					<div className="card-block px-2" style={{ margin: '10px' }}>
						<h4 className="card-title text-light">
							<small>#{id}</small> {name[0].toUpperCase() + name.substring(1)}
						</h4>

						{onClick && (
							<div className="card-text w-100" style={{ marginBottom: '12px' }}>
								<div className="row">
									{types.map(t => (
										<div key={'type_' + t.id + '_for_' + id}>
											<span
												className="btn-sm border border-dark w-100"
												style={{
													backgroundColor: Utils.darkenHexColor(Utils.getColorFromType(t.name)),
													color: '#FFFFFF',
													marginLeft: '30px'
												}}>
												<b>{t.name.toUpperCase()}</b>
											</span>
										</div>
									))}
								</div>
							</div>
						)}

						{(modal && (
							<button
								className={'btn btn-sm btn-' + (myPokemonStore.isMy(id) ? 'danger' : 'warning') + ' border border-dark'}
								style={{ marginLeft: '10px' }}
								onClick={() => {
									if (!myPokemonStore.isMy(id)) myPokemonStore.addPokemon(id);
									else myPokemonStore.removePokemon(id);
									this.forceUpdate();
								}}>
								{myPokemonStore.isMy(id) ? 'Remove my pokemon' : 'Add to my pokemon'}
							</button>
						)) || (
							<button className="btn btn-primary border border-dark" onClick={onClick} data-toggle="modal" data-target="#pokemonCard">
								STATS
							</button>
						)}
					</div>
				</div>
			</div>
		);
	}
}
