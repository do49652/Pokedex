import React from 'react';
import PropTypes from 'prop-types';

/** ### Navigation bar
 */
const Navbar = props => (
	<nav className="navbar navbar-expand-md navbar-dark bg-dark">
		<div className="navbar-brand text-info">{props.title}</div>
		<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar">
			<span className="navbar-toggler-icon" />
		</button>

		<div className="collapse navbar-collapse" id="navbar">
			<ul className="navbar-nav mr-auto mt-2 mt-lg-0">
				<li className={'nav-item' + (props.title === 'Pokédex' ? ' active' : '')}>
					<button className="btn btn-dark nav-link" style={{ marginTop: '5px', padding: '5px' }} onClick={() => props.pageStore.setPage('Pokedex')}>
						Pokédex
					</button>
				</li>
				<li className={'nav-item' + (props.title === 'Movedex' ? ' active' : '')}>
					<button className="btn btn-dark nav-link" style={{ marginTop: '5px', padding: '5px' }} onClick={() => props.pageStore.setPage('Movedex')}>
						Movedex
					</button>
				</li>
				<li className={'nav-item' + (props.title === 'My Pokémon' ? ' active' : '')}>
					<button className="btn btn-dark nav-link" style={{ marginTop: '5px', padding: '5px' }} onClick={() => props.pageStore.setPage('MyPokemon')}>
						My Pokémon
					</button>
				</li>
			</ul>
		</div>
	</nav>
);
Navbar.propTypes = {
	/** page title, shown at the top */
	title: PropTypes.string,
	pageStore: PropTypes.object
};
export default Navbar;
