import React from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';

import * as Utils from '../utils/Utils';

/** ### Pagination
 * Dropdown button that controls number of cards on a page and buttons for navigating through pages.
 */
@observer
export default class Pagination extends React.Component {
	static propTypes = {
		store: PropTypes.oneOfType([Utils.PropType.MovedexStore, Utils.PropType.PokedexStore])
	};
	changePerPage(perPage) {
		const { store } = this.props;
		store.perPage = perPage;
		store.currentPage = 1;
	}
	changePage(page) {
		const { store } = this.props;
		if (page < 1) store.currentPage = 1;
		else if ((page - 1) * store.perPage >= store.filtered.length) {
			while ((page - 1) * store.perPage >= store.filtered.length || page === 1) page -= 1;
		} else store.currentPage = page;
	}
	render() {
		const { store } = this.props;
		return (
			<div className="row">
				<div className="col">
					<div className="dropdown">
						<button className="btn btn-primary dropdown-toggle border border-dark" type="button" data-toggle="dropdown">
							{store.perPage} per page
						</button>
						<div className="dropdown-menu">
							{[10, 20, 50, 100, 10000].map(n => (
								<button className="dropdown-item" key={n} onClick={() => this.changePerPage.bind(this)(n)}>
									{n}
								</button>
							))}
						</div>
					</div>
				</div>

				<div className="col">
					<div className="btn-group w-100">
						<button className="btn btn-primary border border-dark w-100" onClick={() => this.changePage.bind(this)(store.currentPage - 1)}>
							{'<'}
						</button>
						<span className="spn bg-primary text-light border border-dark w-100" style={{ borderRadius: 0 }}>
							{store.currentPage}
						</span>
						<button className="btn btn-primary border border-dark w-100" onClick={() => this.changePage.bind(this)(store.currentPage + 1)}>
							{'>'}
						</button>
					</div>
				</div>
			</div>
		);
	}
}
