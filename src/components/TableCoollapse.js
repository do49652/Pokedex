import React from 'react';
import PropTypes from 'prop-types';

import * as Utils from '../utils/Utils';

/** ### Table Collapse 
 * Collapsable panel. cool
*/
const TableCoollapse = props => (
	<div className="table-sm table-responsive table-borderless rounded border border-dark" style={props.style}>
		<table className="table" style={{ marginBottom: 0 }}>
			<thead>
				<tr>
					<th
						className="text-center border-bottom border-dark text-light clickable-th hoverDarken"
						colSpan="3"
						style={{ backgroundColor: Utils.darkenHexColor(props.style.backgroundColor) }}
						data-toggle="collapse"
						href={'#collapse_' + props.id}
						onClick={props.onClick}>
						{props.title}
					</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td colSpan="3" style={{ padding: 0 }}>
						<div className="collapse" id={'collapse_' + props.id}>
							<div className="container-fluid" style={{ padding: 0 }}>
								<div className="table-sm table-responsive table-borderless text-light">{props.children}</div>
							</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
);
TableCoollapse.propTypes = {
	/** unique DOM id */
	id: PropTypes.string,
	/** main button text */
	title: PropTypes.string,
	/** style to apply, use this for backgroundColor */
	style: PropTypes.object,
	/** run when use clicks on the main button */
	onClick: PropTypes.func,
	/** content */
	children: PropTypes.element.isRequired
};
export default TableCoollapse;
