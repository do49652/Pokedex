import { observable, action, computed } from 'mobx';

import * as Utils from '../utils/Utils';

export default class MovedexStore {
	/** pokémon to show,
	 * array of pokémon ids
	 * if null all ids are shown
	 */
	specificIds = null;
	/** type
	 * @type {string} */
	@observable type = null;
	/** search query, can be name or id */
	@observable search = '';
	@observable currentPage = 1;
	/** number of cards per page */
	@observable perPage = 10;
	/** all moves */
	@observable moves = [];

	constructor(specificIds) {
		this.specificIds = specificIds;
	}

	/** filter moves by specificIds, type and name */
	@computed
	get filtered() {
		const rx = new RegExp(this.search.replace(/^#?/, ''), 'i');
		let moves = [];
		if (this.specificIds) moves = this.moves.filter(p => this.specificIds.indexOf(p.id) !== -1);
		moves = (this.specificIds ? moves : this.moves)
			.filter(p => this.search === '' || rx.test(p.name) || rx.test(p.id.padStart(3, '0')))
			.filter(p => !this.type || p.types.find(t => t.name === this.type));
		return moves;
	}

	/** get moves for current page */
	@computed
	get paginated() {
		const from = (this.currentPage - 1) * this.perPage;
		let to = this.currentPage * this.perPage;
		to = to > this.filtered.length ? this.filtered.length : to;
		return this.filtered.slice(from, to);
	}

	/** fetches move details (pp, accuracy, poer, description...) */
	@action
	async getMoveDetails(P, id) {
		const move = this.moves.find(p => p.id === id);
		if (!move) throw new Error('no move with id');
		if (move.loaded) return this.moves;

		try {
			const moveDetails = await P.getMoveByName(id);
			if (!moveDetails) throw new Error('no move details');

			const effectsEntry = moveDetails.effect_entries.find(entry => entry.language.name === 'en');
			if (!effectsEntry) throw new Error('no move effect entry in english for id');

			const descriptionEntry = moveDetails.flavor_text_entries.find(entry => entry.language.name === 'en');
			if (!descriptionEntry) throw new Error('no move text entry in english for id');

			move.pp = moveDetails.pp;
			move.accuracy = moveDetails.accuracy;
			move.power = moveDetails.power;
			move.effectChance = moveDetails.effect_chance;
			move.damageClass = moveDetails.damage_class.name;
			move.description = descriptionEntry.flavor_text;
			move.shortEffect = effectsEntry.short_effect.replace(/\$effect_chance%/g, move.effectChance + '%');
			move.effect = effectsEntry.effect.replace(/\$effect_chance%/g, move.effectChance + '%');

			move.loaded = true;
		} catch (err) {
			console.log('pokeapi response error', err);
		}
		return this.moves;
	}

	/** fetches all moves */
	@action
	async getMoves(P) {
		try {
			const types = (await P.getTypesList()).results.map(t => ({
				id: Utils.getIdFromUrl(t.url),
				name: t.name
			}));

			const movesTypes = {};

			const typesIds = types.map(t => t.id);
			const typeFetcher = async i => {
				types[i] = await P.getTypeByName(typesIds[i]);

				types[i].moves.forEach(m => {
					const id = Utils.getIdFromUrl(m.url);
					if (!movesTypes[id]) movesTypes[id] = [];
					movesTypes[id].push({ id, name: types[i].name });
				});
				if (i + 1 < typesIds.length) await typeFetcher(i + 1);
			};
			await typeFetcher(0);

			this.moves = (await P.getMovesList()).results.map(m => {
				const id = Utils.getIdFromUrl(m.url);
				return {
					id,
					name: m.name,
					types: movesTypes[id]
				};
			});
		} catch (err) {
			console.log('pokeapi response error', err);
		}
		return this.moves;
	}
}
