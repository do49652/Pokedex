import { observable, action } from 'mobx';
import localStorage from 'mobx-localstorage';

export default class MyPokemonStore {
	/** pokémon ids marked as my */
	@observable myPokemonIds = [];

	/** gets saved pokémon from localStorage */
	constructor() {
		this.myPokemonIds = localStorage.getItem('myPokemon') || [];
	}

	@action
	addPokemon(id) {
		if (!this.isMy(id)) this.myPokemonIds.push(id);
		localStorage.setItem('myPokemon', this.myPokemonIds);
	}

	@action
	removePokemon(id) {
		if (this.isMy(id)) this.myPokemonIds.splice(this.myPokemonIds.indexOf(id), 1);
		localStorage.setItem('myPokemon', this.myPokemonIds);
	}

	@action
	isMy(id) {
		return this.myPokemonIds.indexOf(id) !== -1;
	}
}
