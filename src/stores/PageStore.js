import { observable, action } from 'mobx';

export default class PageStore {
	@observable page = 'Pokedex';
	pokedexStore = null;

	constructor(pokedexStore) {
		this.pokedexStore = pokedexStore;
	}

	@action
	setPage(page) {
		this.page = page;
		this.pokedexStore.myPokemon = page === 'MyPokemon';
	}
}
