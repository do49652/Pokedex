import { observable, action, computed } from 'mobx';

import * as Utils from '../utils/Utils';

export default class PokedexStore {
	/** type
	 * @type {string} */
	@observable type = null;
	/** search query, can be name or id */
	@observable search = '';
	@observable currentPage = 1;
	/** number of cards per page */
	@observable perPage = 10;
	/** all pokémon */
	@observable pokemon = [];
	/** pokémon opened in modal */
	@observable selectedPokemon = null;
	/** total number of types, used for loading pokémon */
	@observable totalNofTypes = 0;
	/** number of fetched types, used for loading pokémon */
	@observable NofTypes = 0;
	/** is this my pokémon */
	@observable myPokemon = false;
	myPokemonStore = null;

	constructor(myPokemonStore) {
		this.myPokemonStore = myPokemonStore;
	}

	/** filter pokémon by myPokemon, type and name */
	@computed
	get filtered() {
		const rx = new RegExp(this.search.replace(/^#?/, ''), 'i');
		return this.pokemon
			.filter(p => p.types.length > 0)
			.filter(p => !this.myPokemon || this.myPokemonStore.isMy(p.id))
			.filter(p => this.search === '' || rx.test(p.name) || rx.test(p.id.padStart(3, '0')))
			.filter(p => !this.type || p.types.find(t => t.name === this.type));
	}

	/** get pokémon for current page */
	@computed
	get paginated() {
		const from = (this.currentPage - 1) * this.perPage;
		let to = this.currentPage * this.perPage;
		to = to > this.filtered.length ? this.filtered.length : to;
		return this.filtered.slice(from, to);
	}

	/** fetch pokémon details and show it in modal */
	@action
	async selectPokemon(P, id, ref) {
		const pokemon = this.pokemon.find(p => p.id === id);
		if (!pokemon) throw new Error('no pokemon with id');
		if (pokemon.loaded) {
			this.selectedPokemon = pokemon;
			await ref.forceUpdate();
			return pokemon;
		}
		this.selectedPokemon = pokemon;
		try {
			const pokemonDetails = await P.getPokemonByName(id);
			if (!pokemonDetails) throw new Error('no pokemon details');

			pokemon.height = pokemonDetails.height / 10;
			pokemon.weight = pokemonDetails.weight / 10;
			pokemon.abilities = pokemonDetails.abilities.map(a => ({ hidden: a.is_hidden, id: Utils.getIdFromUrl(a.ability.url), name: a.ability.name }));
			pokemon.stats = pokemonDetails.stats.map(s => ({ id: Utils.getIdFromUrl(s.stat.url), name: s.stat.name, value: s.base_stat }));
			pokemon.maxStat = Math.max(...pokemon.stats.map(s => s.value));
			pokemon.moves = [...new Set(pokemonDetails.moves.map(m => Utils.getIdFromUrl(m.move.url)))];
			await ref.forceUpdate();

			const speciesDetails = await P.getPokemonSpeciesByName(Utils.getIdFromUrl(pokemonDetails.species.url));
			if (!speciesDetails) throw new Error('no pokemon species');

			const descriptionEntry = speciesDetails.flavor_text_entries.find(entry => entry.language.name === 'en');
			if (!descriptionEntry) throw new Error('no pokemon text entry in english for id');

			pokemon.description = descriptionEntry.flavor_text;
			await ref.forceUpdate();

			const generaEntry = speciesDetails.genera.find(entry => entry.language.name === 'en');
			if (!generaEntry) throw new Error('no pokemon genera entry in english for id');

			pokemon.species = generaEntry.genus;
			await ref.forceUpdate();

			pokemon.loaded = true;
		} catch (err) {
			console.log('pokeapi response error', err);
			this.selectedPokemon = null;
		}
		this.selectedPokemon = pokemon;
		await ref.forceUpdate();
		return pokemon;
	}

	/** fetches all pokémon */
	@action
	async getPokemon(P) {
		try {
			const types = (await P.getTypesList()).results.map(t => ({
				id: Utils.getIdFromUrl(t.url),
				name: t.name
			}));

			const typesIds = types.map(t => t.id);
			this.totalNofTypes = typesIds.length;

			const pokemons = {};
			(await P.getPokemonsList()).results.filter(p => Utils.getIdFromUrl(p.url) < 10091).forEach(p => {
				pokemons[Utils.getIdFromUrl(p.url)] = { id: Utils.getIdFromUrl(p.url), name: p.name, types: [] };
			});

			const typeFetcher = async i => {
				this.NofTypes += 1;
				types[i] = await P.getTypeByName(typesIds[i]);

				types[i].pokemon.forEach(p => {
					const id = Utils.getIdFromUrl(p.pokemon.url);
					if (!pokemons[id]) return;
					pokemons[id].types.push({ id: types[i].id, name: types[i].name });
				});
				this.pokemon = Object.values(pokemons);
				if (i + 1 < typesIds.length) await typeFetcher(i + 1);
			};
			await typeFetcher(0);
		} catch (err) {
			console.log('pokeapi response error', err);
		}
		return this.pokemon;
	}
}
